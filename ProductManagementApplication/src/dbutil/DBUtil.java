package dbutil;

import java.sql.*;

public class DBUtil {

	public static Connection getConnection()
	{
		Connection conn = null;
		try 
		{
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//		    conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","sys","sys");
		    
		    String driverName = "oracle.jdbc.driver.OracleDriver";
		    Class.forName(driverName).newInstance();
		    String nameForConnect = "sys as sysdba";
		    String pass = "sys";
		    String url = "jdbc:oracle:thin:@192.168.1.226:1521:ORCL";
		     conn = DriverManager.getConnection(url, nameForConnect, pass);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return conn;
	}
	
	
	public static void closeConnection(Connection conn)
	{
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
